from PyQt5 import QtCore, QtGui, QtWidgets
import sys
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QLabel
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QApplication, QPushButton
from PyQt5.QtWidgets import QInputDialog
import csv
import os
import subprocess

if os.access('C:\Keeper_of_the_keys', os.F_OK):
    os.chdir('C:\Keeper_of_the_keys')
    ind_password = True


else:
    os.mkdir('C:\Keeper_of_the_keys')
    os.chdir('C:\Keeper_of_the_keys')
    ind_password = False
    csvfile = open('Keeper_of_the_keys.csv', 'a', encoding='utf8')
    csvfile.write('')
    csvfile.close()
    subprocess.call(['attrib', '+h', "C:\Keeper_of_the_keys"])


def cod(str):
    key = 6
    encript_str = ""
    for letter in str:
        key+=1
        encript_str += chr(ord(letter) ^ key)
    return encript_str


def go():
    class Ui_MainWindow(object):
        def setupUi(self, MainWindow):
            MainWindow.setObjectName("MainWindow")
            MainWindow.resize(1176, 495)
            MainWindow.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
            self.centralwidget = QtWidgets.QWidget(MainWindow)
            self.centralwidget.setObjectName("centralwidget")

            ##############start input###############################################
            self.groupBox_input = QtWidgets.QGroupBox(self.centralwidget)
            self.groupBox_input.setEnabled(True)
            self.groupBox_input.setGeometry(QtCore.QRect(30, 30, 201, 341))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.groupBox_input.setFont(font)
            self.groupBox_input.setObjectName("groupBox_input")

            self.button_input = QtWidgets.QPushButton(self.groupBox_input)
            self.button_input.setGeometry(QtCore.QRect(10, 260, 161, 41))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.button_input.setFont(font)
            self.button_input.setObjectName("button_input")

            self.input_comment = QtWidgets.QTextEdit(self.groupBox_input)
            self.input_comment.setGeometry(QtCore.QRect(10, 180, 161, 61))
            self.input_comment.setObjectName("input_comment")

            self.text_input_comment = QtWidgets.QLabel(self.groupBox_input)
            self.text_input_comment.setGeometry(QtCore.QRect(10, 160, 121, 16))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.text_input_comment.setFont(font)
            self.text_input_comment.setObjectName("text_input_comment")

            self.input_parol = QtWidgets.QLineEdit(self.groupBox_input)
            self.input_parol.setGeometry(QtCore.QRect(10, 120, 161, 21))
            self.input_parol.setObjectName("input_parol")

            self.text_login_parol = QtWidgets.QLabel(self.groupBox_input)
            self.text_login_parol.setGeometry(QtCore.QRect(10, 100, 101, 16))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.text_login_parol.setFont(font)
            self.text_login_parol.setObjectName("text_login_parol")

            self.input_lodin = QtWidgets.QLineEdit(self.groupBox_input)
            self.input_lodin.setGeometry(QtCore.QRect(10, 60, 161, 21))
            self.input_lodin.setObjectName("input_lodin")

            self.text_input_login = QtWidgets.QLabel(self.groupBox_input)
            self.text_input_login.setGeometry(QtCore.QRect(10, 40, 141, 16))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.text_input_login.setFont(font)
            self.text_input_login.setObjectName("text_input_login")
            ###########################################################end input######

            #################start found###############################################
            self.groupBox_found = QtWidgets.QGroupBox(self.centralwidget)
            self.groupBox_found.setGeometry(QtCore.QRect(270, 30, 461, 341))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.groupBox_found.setFont(font)
            self.groupBox_found.setObjectName("groupBox_found")

            self.text_fount_login = QtWidgets.QLabel(self.groupBox_found)
            self.text_fount_login.setGeometry(QtCore.QRect(10, 70, 131, 16))
            self.text_fount_login.setObjectName("text_fount_login")

            self.found_login = QtWidgets.QLineEdit(self.groupBox_found)
            self.found_login.setGeometry(QtCore.QRect(10, 90, 151, 21))
            self.found_login.setObjectName("found_login")

            self.text_foung_parol = QtWidgets.QLabel(self.groupBox_found)
            self.text_foung_parol.setGeometry(QtCore.QRect(10, 130, 151, 16))
            self.text_foung_parol.setObjectName("text_foung_parol")

            self.found_parol = QtWidgets.QLineEdit(self.groupBox_found)
            self.found_parol.setGeometry(QtCore.QRect(10, 150, 151, 21))
            self.found_parol.setObjectName("found_parol")

            self.text_found_comment = QtWidgets.QLabel(self.groupBox_found)
            self.text_found_comment.setGeometry(QtCore.QRect(10, 190, 151, 16))
            self.text_found_comment.setObjectName("text_found_comment")

            self.found_comment = QtWidgets.QTextEdit(self.groupBox_found)
            self.found_comment.setGeometry(QtCore.QRect(10, 210, 151, 41))
            self.found_comment.setObjectName("found_comment")

            self.text_comment_1 = QtWidgets.QLabel(self.groupBox_found)
            self.text_comment_1.setGeometry(QtCore.QRect(10, 30, 261, 16))
            font = QtGui.QFont()
            font.setPointSize(11)
            self.text_comment_1.setFont(font)
            self.text_comment_1.setObjectName("text_comment_1")
            self.text_comment_2 = QtWidgets.QLabel(self.groupBox_found)
            self.text_comment_2.setGeometry(QtCore.QRect(10, 50, 251, 16))
            font = QtGui.QFont()
            font.setPointSize(11)
            self.text_comment_2.setFont(font)
            self.text_comment_2.setObjectName("text_comment_2")

            self.button_found = QtWidgets.QPushButton(self.groupBox_found)
            self.button_found.setGeometry(QtCore.QRect(10, 260, 151, 41))
            self.button_found.setObjectName("button_found")

            self.text_comment_3 = QtWidgets.QLabel(self.groupBox_found)
            self.text_comment_3.setGeometry(QtCore.QRect(10, 310, 441, 16))
            font = QtGui.QFont()
            font.setPointSize(11)
            self.text_comment_3.setFont(font)
            self.text_comment_3.setObjectName("text_comment_3")
            self.text_comment_4 = QtWidgets.QLabel(self.groupBox_found)
            self.text_comment_4.setGeometry(QtCore.QRect(10, 320, 321, 16))
            font = QtGui.QFont()
            font.setPointSize(11)
            self.text_comment_4.setFont(font)
            self.text_comment_4.setObjectName("text_comment_4")
            ##################################################end found###############

            ###################start fiund in list########################################
            self.groupBox_fount_in_list = QtWidgets.QGroupBox(self.centralwidget)
            self.groupBox_fount_in_list.setGeometry(QtCore.QRect(760, 30, 391, 121))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.groupBox_fount_in_list.setFont(font)
            self.groupBox_fount_in_list.setObjectName("groupBox_fount_in_list")

            self.list_box_parol = QtWidgets.QComboBox(self.groupBox_fount_in_list)
            self.list_box_parol.setGeometry(QtCore.QRect(8, 30, 375, 31))
            self.list_box_parol.setObjectName("list_box_parol")
            self.list_box_parol.addItem('Все логины и пароли')
            #####################################################end fiund in list###########

            ###############start output######################################################
            self.groupBox_output = QtWidgets.QGroupBox(self.centralwidget)
            self.groupBox_output.setGeometry(QtCore.QRect(760, 170, 391, 201))
            font = QtGui.QFont()
            font.setFamily("Garamond")
            font.setPointSize(12)
            self.groupBox_output.setFont(font)
            self.groupBox_output.setObjectName("groupBox_output")

            self.to_output = QtWidgets.QTextBrowser(self.groupBox_output)
            self.to_output.setGeometry(QtCore.QRect(20, 30, 351, 128))
            self.to_output.setObjectName("to_output")
            MainWindow.setCentralWidget(self.centralwidget)

            self.button_output = QtWidgets.QPushButton(self.groupBox_output)
            self.button_output.setGeometry(QtCore.QRect(20, 164, 351, 25))
            self.button_output.setObjectName("button_found")
            #####################################################end output######################
            self.menubar = QtWidgets.QMenuBar(MainWindow)
            self.menubar.setGeometry(QtCore.QRect(0, 0, 1176, 31))
            self.menubar.setObjectName("menubar")
            MainWindow.setMenuBar(self.menubar)

            self.statusbar = QtWidgets.QStatusBar(MainWindow)
            self.statusbar.setObjectName("statusbar")
            MainWindow.setStatusBar(self.statusbar)

            self.text_error = QtWidgets.QLabel(self.centralwidget)
            self.text_error.setGeometry(QtCore.QRect(40, 400, 1111, 41))
            font = QtGui.QFont()
            font.setPointSize(13)
            font.setBold(False)
            font.setWeight(50)
            self.text_error.setFont(font)
            self.text_error.setText("")
            self.text_error.setObjectName("text_error")
            MainWindow.setCentralWidget(self.centralwidget)

            self.retranslateUi(MainWindow)
            QtCore.QMetaObject.connectSlotsByName(MainWindow)

        def retranslateUi(self, MainWindow):
            _translate = QtCore.QCoreApplication.translate
            MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
            '''MainWindow.setToolTip(
                _translate("MainWindow", "<html><head/><body><p align=\"center\">пнгти</p></body></html>"))
            self.groupBox_input.setWhatsThis(
                _translate("MainWindow", "<html><head/><body><p align=\"center\"> 0шдзхж-0</p></body></html>"))'''

            self.groupBox_input.setTitle(_translate("MainWindow", "Создание новой записи"))
            self.button_input.setText(_translate("MainWindow", "Записать"))
            self.text_input_comment.setText(_translate("MainWindow", "Комментарий"))
            self.text_login_parol.setText(_translate("MainWindow", "Пароль:"))
            self.text_input_login.setText(_translate("MainWindow", "Логин:"))
            self.groupBox_found.setTitle(_translate("MainWindow", "Поиск по вводу"))
            self.text_fount_login.setText(_translate("MainWindow", "Логин:"))
            self.text_foung_parol.setText(_translate("MainWindow", "Пароль:"))
            self.text_found_comment.setText(_translate("MainWindow", "Комментарий:"))
            self.text_comment_1.setText(_translate("MainWindow", "Введите параметры (один или несколько)"))
            self.text_comment_2.setText(_translate("MainWindow", " по которым хотите осуществить поиск "))
            self.button_found.setText(_translate("MainWindow", "Искать"))
            self.text_comment_3.setText(
                _translate("MainWindow", "*если в введённых вами данных будут допущены неточности, "))
            self.text_comment_4.setText(_translate("MainWindow", "  вам будут представлены скожие варианты"))
            self.groupBox_fount_in_list.setTitle(_translate("MainWindow", "Поиск по списку"))
            self.groupBox_output.setTitle(_translate("MainWindow", "Вот что удалось найти:"))
            self.button_output.setText(_translate("MainWindow", "стереть"))

    class MyWidget(QMainWindow, Ui_MainWindow):
        def __init__(self):
            super().__init__()
            self.save_information = []
            self.setupUi(self)
            self.button_input.clicked.connect(self.run_input)
            self.button_found.clicked.connect(self.run_found)
            self.button_output.clicked.connect(self.run_output)
            with open('Keeper_of_the_keys.csv', 'r', encoding="utf8") as csvfile:
                reader = list(csv.reader(csvfile, delimiter=';'))
                self.user_password = cod(reader[0][0])
                #print(reader[0][0])
                for i in reader[0][1:]:
                    self.save_information.append(cod(i).split('~'))
                    #print('//'.join(i.split('~')))
                    self.list_box_parol.addItem('//'.join(cod(i).split('~')))
            #print(self.save_information)



        def run_input(self):
            login = self.input_lodin.text()
            parol = self.input_parol.text()
            comment = self.input_comment.toPlainText()
            if login == '' or parol == '':
                text = txt = '''<font color= "#A52A2A" align="center">Неправильный ввод! Проверьте, заполнены ли поля ввода логина и пароля.</font>'''
                self.text_error.setText(text)
            else:
                csvfile = open('Keeper_of_the_keys.csv', 'a', encoding='utf8')
                csvfile.write(';'+cod(login+'~'+parol+'~'+comment))
                csvfile.close()
                self.text_error.setText('')
                self.input_parol.setText('')
                self.input_lodin.setText('')
                self.input_comment.setText('')

                self.save_information.append([login, parol, comment])
                self.list_box_parol.addItem(login + '//' + parol + '//' + comment)

        def run_found(self):
            self.ind_to_found = False
            login = self.found_login.text()
            parol = self.found_parol.text()
            comment = self.found_comment.toPlainText()
            for i in self.save_information:
                if login == i[0] or parol == i[1] or comment == i[2]:
                    i = 'логин: ' + i[0] + '\nпароль: ' + i[1] + '\nкомментарий: ' + i[2] + '\n'
                    self.to_output.append(i)
                    self.found_parol.setText('')
                    self.found_login.setText('')
                    self.found_comment.setText('')
                    self.ind_to_found = True

            if not self.ind_to_found:
                self.to_output.append('таких записей не существует')
                self.found_parol.setText('')
                self.found_login.setText('')
                self.found_comment.setText('')
            self.ind_to_found = False

        def run_output(self):
            self.to_output.setText('')

    app = QApplication(sys.argv)
    ex = MyWidget()
    ex.show()
    sys.exit(app.exec_())


class Example(QWidget):
    def get_parol_key(self):
        password1, okBtnPressed = QInputDialog.getText(self, "1 ввод",
                                                       "Для дальнейшего функцианирования программы введите пароль, \nон будет запрашиваться каждый раз, \nкогда вы будете запускать программу:")
        while password1 == '':
            password1, okBtnPressed = QInputDialog.getText(self, "1 ввод",
                                                           "Пустая строка не может являться паролем. Введите новый пароль:")
        password2, okBtnPressed = QInputDialog.getText(self, "2 ввод", "Введите пароль ещё один раз:")
        while password1 != password2:
            password1, okBtnPressed = QInputDialog.getText(self, "1 ввод",
                                                           "Неверный второй пароль. Введите новый пароль:")
            password2, okBtnPressed = QInputDialog.getText(self, "2 ввод",
                                                           "Введите новый пароль ещё один раз:")

        csvfile = open('Keeper_of_the_keys.csv', 'a', encoding='utf8')
        csvfile.write(cod(password1))
        csvfile.close()
        self.parol_key = cod(password2)
        #print('get ney password')

    def get_parol(self):
        i, okBtnPressed = QInputDialog.getText(self, "Защита доступа", "Введите пароль:")
        ind = 1
        while i != cod(self.parol_key):
            ind += 1
            i, okBtnPressed = QInputDialog.getText(self, "Защита", "Пароль неверный \nВведите пароль:")
            if ind == 3:
                self.setGeometry(300, 300, 300, 300)
                self.setWindowTitle('Диалоговые окна')
                self.text = QLabel(self)
                self.text.move(25, 25)
                text = '''<font color= "#A52A2A" align="center">Слишком много попыток <br> введения пароля! <br><br> Вход запрещён.</font>'''
                self.text.setText(text)
                font = QtGui.QFont()
                font.setPointSize(14)
                self.text.setFont(font)
                self.show()
                break
        if i == cod(self.parol_key):
            go()

    def __init__(self):
        super().__init__()
        with open('Keeper_of_the_keys.csv', 'r', encoding="utf8") as csvfile:
            reader = list(csv.reader(csvfile, delimiter=';'))
        if len(reader) == 0:
            self.get_parol_key()
            go()
        else:
            self.parol_key = reader[0][0]
            #print(self.parol_key, reader)
            self.get_parol()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
