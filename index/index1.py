from flask import Flask
from PIL import Image
from random import randint

app = Flask(__name__)


@app.route('/')
@app.route('/bootstrap_sample')
def bootstrap():
    return '''<!doctype html>
                <!DOCTYPE html>
                <html lang="en">
                    <head>
                      <title>Пример галереи "Карусель"</title>
                      <meta charset="utf-8">
                      <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
                      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
                      <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
                      <style>
                      .carousel-inner > .item > img,
                      .carousel-inner > .item > a > img {
                          margin: auto;
                          width: 70%;

                      }
                      body{background: #B0E0E6}
                      </style>
                    </head>
                  <body bgcolor = “#002902”>
                    <h1 align = 'center' >Привет, Яндекс! Я - Влада</h1>
                        <nav class="navbar navbar-inverse">
                          <div class="container-fluid">
                            <div  class="navbar-header">
                                <ul class="nav navbar-nav">
                                <li>................................</li>
                                  <li><a href='https://ru.wikipedia.org/wiki/%D0%94%D0%B5%D0%BF%D0%BF,_%D0%94%D0%B6%D0%BE%D0%BD%D0%BD%D0%B8'><button type="/image_sample1" class="btn btn-default">Джонни Депп</button></a></li>
                                  <li>.....................</li>
                                  <li><a href=""><button type="/image_sample2" class="btn btn-primary">Джеки Чан</button></a></li> 
                                  <li>..........</li>
                                  <li><a href="https://ru.wikipedia.org/wiki/%D0%9A%D0%B2%D0%B0%D1%80%D1%82%D0%B5%D1%82_%D0%98"><button type="/image_sample3" class="btn btn-info">Квартет И</button></a></li> 
                                  <li>................................</li>
                                </ul>
                            </div>
                            <ul class="nav navbar-nav">
                            <li>.............................</li>
                              <li><a href="https://www.kinopoisk.ru/film/piraty-karibskogo-morya-proklyatie-chernoy-zhemchuzhiny-2003-4374/">Пиаты Карибского моря</a></li>
                              <li>.....................</li>
                              <li><a href="https://www.kinopoisk.ru/film/829/">Шанхайский полдень</a></li> 
                              <li>.................</li>
                              <li><a href="https://www.kinopoisk.ru/film/den-vyborov-2007-309408/">День выборов</a></li> 
                              <li>..............................</li>
                            </ul>
                          </div>
                        </nav>                       

                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                          <li data-target="#myCarousel" data-slide-to="0" ></li>
                          <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
                          <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">

                          <div class="item">
                            <img src="static/imgg/jack.jpg" alt="0">
                            <div class="carousel-caption">
                              <h3>Джони Депп</h3>
                            </div>
                          </div>

                          <div class="item active">
                            <img src="static/imgg/tetra_i.jpg" alt="1">
                            <div class="carousel-caption">
                              <h3>Квартет И</h3>
                            </div>
                          </div>

                          <div class="item">
                            <img src="static/imgg/jackie.jpg" alt="2">
                            <div class="carousel-caption">
                              <h3>Джеки Чан</h3>
                            </div>
                          </div>

                        </div>
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                    <div class="alert alert-success">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                      <strong>Предупреждение!</strong> Вы кажется ошиблись адресом...
                    </div>
                  </body>
                </html>'''


@app.route('/image_sample1')
def image1():
    return '''<div><img src="static/imgg/jack.jpg" alt="здесь должна была быть
    картинка, но не нашлась"></div>'''


@app.route('/image_sample2')
def image2():
    return '''<img src="static/imgg/jackie_and_owen.jpg" alt="1здесь должна была быть
    картинка, но не нашлась">'''


@app.route('/image_sample3')
def image3():
    return '''
    <img src="static/imgg/tetra_i.jpg" 
     alt="здесь должна была быть картинка, но не нашлась">'''


youtube_html = ['''
    <!DOCTYPE HTML>
        <html>
         <head>
          <meta charset="utf-8">
          <title>Тег IFRAME</title>
          <style>
          body{background: #B0E0E6}   
          .foo {
            width: 1000px;
            height: 700px;
            background: url('http://vmuseum.ucoz.ru/img/tel.png') no-repeat;
            position: relative;
            }
        .foo .bar {
            position: absolute;
            left: 45px;
            top: 43px;
            }
          </style>
         </head>
         <body>  
         <div class="foo">
            <div class="bar">
            <iframe alt = 'опс..' src="''', '''"?rel=0&amp;showinfo=0&amp;modestbranding=0&amp;theme=light&amp;autohide=1&amp;html5=1" width="300" height="225" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
            </div>     
         </div>
         </body>
        </html>
    ''']


@app.route('/youtube/<param>')
def youtube(param):
    dictt = {'1': 'https://www.youtube.com/embed/Qni2meCZRek',
             '2': 'https://www.youtube.com/embed/vX--20kUnQs',
             '3': 'https://www.youtube.com/embed/M8_AKTffII0',
             '4': 'https://www.youtube.com/embed/-juJWVfhbSY',
             '5': 'https://www.youtube.com/embed/6hi0etcrqO8'}
    return youtube_html[0] + dictt[param] + youtube_html[1]


@app.route('/yandex_music/')
def yandex_music():
    return '''
        <!DOCTYPE HTML>
            <html>
             <head>
              <meta charset="utf-8">
              <title>Тег IFRAME</title>
              <style>
              body{background: #B0E0E6}   
              </style>
             </head>
             <body>  
             <div>
                <iframe frameborder="0" style="border:none;width:600px;height:100px;" width="600" height="100" src="https://music.yandex.ru/iframe/#track/267291/3869584/">Слушайте <a href='https://music.yandex.ru/album/3869584/track/267291'>The Medallion Calls</a> — <a href='https://music.yandex.ru/artist/45101'>Klaus Badelt</a> на Яндекс.Музыке</iframe>    
             </div>
             </body>
            </html>
        '''

html = ['''
        <!DOCTYPE HTML>
            <html>
             <head>
              <meta charset="utf-8">
              <title>Тег IFRAME</title>
                <link rel="stylesheet"
                href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
                crossorigin="anonymous">
              <style>
              body{background: #B0E0E6}   
              </style>
             </head>
             <body>  
             <div>
                ''', '''
             </div>
             </body>
            </html>
        ''']

@app.route('/list/<param>')
def listik(param):
    html_little = html.copy()
    html_little[0]+='<ul>'
    html_little[1] = '</ul>'+html_little[1]
    for i in range(1, int(param)+1):
        html_little[0] += '<li>{}</li>'.format(str(i))
    return html_little[0] + html_little[1]

@app.route('/table/<th>/<tr>')
def table_(th, tr):
    def plas(s, text=''):
        html_little[0] += s+text
        html_little[1] = s[0]+'/'+s[1:]+html_little[1]
    html_little = html.copy()
    plas('<table border = 1>')
    for y in range(1, int(tr)+1):
        html_little[0] += '<tr>'
        for x in range(1, int(th)+1):
            plas('<th>', str(x)+';'+str(y))
    return html_little[0]+html_little[1]


@app.route('/image_puzzle/<num>')
def image_puzzle(num):
    def plas(s, text=''):
        html_little[0] += s+text
        html_little[1] = s[0]+'/'+s[1:]+html_little[1]
    html_little = html.copy()
    plas('<table>')
    im = Image.open('static/puzzle.jpg')
    x, y = im.size
    x = x//4
    y = y//4
    num =int(num)-1
    hole = (num//4, num%4)
    for i in range(4):
        html_little[0] += '<tr>'
        for j in range(4):
            if (i, j) != hole:
                little_img = im.crop((x*j, y*i, x*(j+1), y*(i+1)))
                little_img.save('static/'+str(i)+str(j)+'.jpg')
                html_little[0] += '<th><img src="static/'+str(num)+str(i)+str(j)+'.jpg'+'"></th>'
            else:
                html_little[0] += '<th></th>'
        html_little[0] += '</tr>'
    return ''.join(html_little)

@app.route("/text_in_alert/<text>")
def text_in_alert(text):
    return html[0]+'''<div style="margin: 15px;" class="alert alert-danger" role="alert">{}</div>'''.format(text)+html[1]



if __name__ == '__main__':
    app.run(port=8507, host='127.0.0.1')
